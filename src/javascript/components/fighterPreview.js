import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  function createPreviewHeroImage(source) {
    const attributes = { src: source };
    const imgElement = createElement({
      tagName: 'img',
      className: 'fighter-preview___img',
      attributes
    });

    if(position === 'right') {
      imgElement.style.transform = 'scale(-1, 1)';
    }
  
    return imgElement;
  }

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function createPreviewHeroProperty(prop) {
    const nameElement = createElement({ 
      tagName: 'label',
      className: 'fighter-preview___propetry'
    });
    nameElement.innerText = capitalizeFirstLetter(prop.join(': '));
  
    return nameElement;
  }

  if(fighter) {
    const {source} = fighter;
    const fighterList = Object.entries(fighter);
    fighterElement.append(createPreviewHeroImage(source));
    fighterList
      .filter(prop => !prop.includes('_id') && !prop.includes('source'))
      .forEach(prop => fighterElement.append(createPreviewHeroProperty(prop)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
