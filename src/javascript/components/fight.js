import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const healthWraps = document.getElementsByClassName('arena___health-bar');
    const healthStatusWraps = document.getElementsByClassName('arena___health-indicator');
    const [healthWrapFirstPlayer, healthWrapSecondPlayer] = healthWraps;
    const [healthStatusFirstPlayer, healthStatusSecondPlayer] = healthStatusWraps;
    const statusInfo = {
      block: false,  
      currentHealth: 100,
      timeOfCrit: Date.now(),
      critInput: []
    }

    const firstPlayer = { 
      ...firstFighter, 
      ...statusInfo, 
      healthBar: healthWrapFirstPlayer, 
      statusView: healthStatusFirstPlayer,
      position: 'left',
    }

    const secondPlayer = { 
      ...secondFighter, 
      ...statusInfo, 
      healthBar: healthWrapSecondPlayer, 
      statusView: healthStatusSecondPlayer,
      position: 'right',
    }

    function attackRelease(attacker, defender) {
      const totalDamage = getDamage(attacker, defender);

      defender.currentHealth = defender.currentHealth - totalDamage / defender.health * 100;
      if(defender.currentHealth < 0) {
        document.removeEventListener('keydown', onDown);
        document.removeEventListener('keyup', onUp);
        resolve(attacker);
      }

      defender.healthBar.style.width = `${defender.currentHealth}%`;
    }

    function critHandler(fighter, code) {
      const currentTime = Date.now();
      if(currentTime - fighter.timeOfCrit < 10000) {
        return false;
      }

      if(!fighter.critInput.includes(code)) {
        fighter.critInput.push(code);
        return false;
      }

      if(fighter.critInput.length === 3) {
        fighter.timeOfCrit = currentTime;
        return true;
      }
    }

    function onDown({code}) {
      switch(code) {
        case controls.PlayerOneAttack: 
            attackRelease(firstPlayer, secondPlayer);
          break;

        case controls.PlayerTwoAttack: 
            attackRelease(secondPlayer, firstPlayer);
          break;
        
        case controls.PlayerOneBlock: 
            firstPlayer.block = true;
          break;

        case controls.PlayerTwoBlock: 
            secondPlayer.block = true;
          break;
      }



      if(controls.PlayerOneCriticalHitCombination === firstPlayer.critInput) {
        critHandler(firstPlayer, code) ? attackRelease(firstPlayer, secondPlayer) : null;
      }

      if(controls.PlayerTwoCriticalHitCombination === secondPlayer.critInput) {
        critHandler(secondPlayer, code) ? attackRelease(secondPlayer, firstPlayer) : null;
      }
    }

    function onUp({code}) {
      switch(code) {
        case controls.PlayerOneBlock: 
            firstPlayer.block = false; 
          break;

        case controls.PlayerTwoBlock: 
            secondPlayer.block = false; 
          break;
      }

      if(firstPlayer.critInput.includes(code)) {
        firstPlayer.critInput.splice(firstPlayer.critInput.indexOf(code), 1);
      }

      if(secondPlayer.critInput.includes(code)) {
        secondPlayer.critInput.splice(secondPlayer.critInput.indexOf(code), 1);
      }
    }

    document.addEventListener('keyup', onUp);
    document.addEventListener('keydown', onDown);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = fighter.critInput === 3 ? 2 : Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
