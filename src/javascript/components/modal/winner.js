import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // call showModal function 
  const winnerInfo = {
    title: 'The fight is over!',
    bodyElement: `Won by hero: ${fighter.name}`
  }
  
  showModal(winnerInfo);
}
